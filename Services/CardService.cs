﻿using Donatello.Infrastructure;
using Donatello.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Donatello.Services
{
    public class CardService
    {
        private readonly DonatelloContext context;

        public CardService(DonatelloContext context)
        {
            this.context = context;
        }

        public CardDetails GetDetails(int id)
        {
            var card = context
                .Cards
                .Include(c => c.Column) // 1. add this line
                .SingleOrDefault(x => x.Id == id);
            if (card == null)
                return new CardDetails();

            // 2. retrieve the board
            var board = context
                .Boards
                .Include(b => b.Columns)
                .SingleOrDefault(b => b.Id == card.Column.BoardId);

            // 3. map the boards columns
            var availableColumns = board
                .Columns
                .Select(x => new SelectListItem
                {
                    Text = x.Title,
                    Value = x.Id.ToString()
                });

            return new CardDetails
            {
                Id = card.Id,
                Contents = card.Contents,
                Notes = card.Notes,
                Columns = availableColumns.ToList(), // 4. available columns
                Column = card.ColumnId // 5. current column
            };
        }

        public void Update(CardDetails cardDetails)
        {
            var card = context.Cards.SingleOrDefault(x => x.Id == cardDetails.Id);
            card.Contents = cardDetails.Contents;
            card.Notes = cardDetails.Notes;
            card.ColumnId = cardDetails.Column;

            context.SaveChanges();
        }
    }
}
